# Comandos

## Node

pnpm run dev -> Roda a aplicação no localhost.

## Prisma

pnpm migrate dev -> Gera as migrations.
pnpm prisma studio -> Abre um Studio para visualizar o banco de dados.
